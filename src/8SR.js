/**
Copyright (c) 2020,2021 PickData SL (https://www.pickdata.net/)
All rights reserved.
emod-js-simulator - The BSD 3-Clause License
**/

class SR8 {
    constructor() {
        this.relay_status = new Array(8).fill(false);
        this.pulse_width = new Array(8).fill(0);
    }

    setPulseWitdth(pulse_width_array) {
        if (!Array.isArray(pulse_width_array) || pulse_width_array.length != 8) {
            throw new Error('pulse_width_array must be an array of 8 elements');
        }

        for (var i = 0; i < 8; i++) {
            if (Number.isInteger(pulse_width_array[i]) && pulse_width_array[i] < 0) {
                this.pulse_width[i] = pulse_width_array[i];
            } else {
                // emod-controller-js-bindings equivalent method does implicit conversion to 0 if value is not a valid integer.
                this.pulse_width[i] = 0;
            } 
        }
    }

    setOne(index, value) {
        if (!Number.isInteger(index) || index < 0 || index > 7) {
            throw new Error('index must be an integer between 0 and 7');
        }

        if (typeof value !== "boolean") {
            throw new Error('value must be a boolean');
        }

        this.relay_status[index] = value;

        if (this.relay_status[index] && this.pulse_width[index] != 0) {
            setTimeout(() => {
                this.relay_status[index] = false;
            }, this.pulse_width[index]);
        }
    }

    setAll(value) {
        if (typeof value !== "boolean") {
            throw new Error('value must be a boolean');
        }

        for (var i = 0; i < 8; i++) {
            this.setOne(i, value);
        }
    }

    set(new_relay_status) {
        if (!Array.isArray(new_relay_status) || new_relay_status.length != 8) {
            throw new Error('new_relay_status must be an array of 8 elements');
        }

        for (var i = 0; i < 8; i++) {
            if (typeof new_relay_status[i] === "boolean") {
                this.setOne(i, new_relay_status[i]);
            }            
        }
    }

    getAll() {
        const relay_status = [...this.relay_status];

        return relay_status;
    }

    getOne(index) {
        if (!Number.isInteger(index) || index < 0 || index > 7) {
            throw new Error('index must be an integer between 0 and 7');
        }

        const relay_status = this.getAll();

        return relay_status[index];
    }
}


class Singleton8SR {
    constructor() {
        throw new Error('Use getInstance(num)');
    }
    
    static getInstance(num) {
        if (!Number.isInteger(num) || num < 1 || num > 5) {
            throw new Error('Module number must be an integer between 1 and 5');
        }

        if (!Singleton8SR.instance) {
            Singleton8SR.instance = new Array(5).fill(null);;
        }

        if (!Singleton8SR.instance[num-1]) {
            Singleton8SR.instance[num-1] = new SR8();
        }

        return Singleton8SR.instance[num-1];
    }
}

module.exports = Singleton8SR;
