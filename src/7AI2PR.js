/**
Copyright (c) 2020,2021 PickData SL (https://www.pickdata.net/)
All rights reserved.
emod-js-simulator - The BSD 3-Clause License
**/

class AI7PR2 {
    constructor() {
        this.relay_status = new Array(2).fill(false);
        this.relay_pulse_width = new Array(2).fill(0);

        this.inputs_value = new Array(7).fill(0.0);
        this.max_input_value = 12.0;
        this.min_input_value = 0.0;
        this.input_value_change_threshold = 0.250;
        this.last_reported_threshold_value = new Array(7).fill(-1);
        this.input_value_change_threshold_callback = null;
        this.input_value_change_direction = new Array(7).fill(1);
        this.input_value_change_timer = new Array(7).fill(null);
        this.input_value_change_timer_min = new Array(7).fill(250);
        this.input_value_change_timer_max = new Array(7).fill(1500);

        this.input_value_range_low = 1.0;
        this.input_value_range_high = 3.3;
        this.last_input_value_in_range = new Array(7).fill(false);
        this.within_range_callback = null;
        this.out_of_range_callback = null;

        this.interval_callback = null;
        this.interval_timer = null;
    }

    stopSimulation() {
        for (var i = 0; i < 7; i++) {
            if (this.input_value_change_timer[i] != null) {
                clearTimeout(this.input_value_change_timer[i]);
                this.input_value_change_timer[i] = null;
            }
        }
    }

    startSimulation() {
        this.stopSimulation();
        this.inputs_value.fill(0.0);
        for (var i = 0; i < 7; i++) {
            this.input_value_change_timer[i] = setTimeout(this._inputChange.bind(this), this._inputsGetChangeTimeout(i), i);
        }
    }

    _inputsGetChangeTimeout(index) {
        if (!Number.isInteger(index) || index < 0 || index > 7) {
            throw new Error('index must be an integer between 0 and 7');
        }

        return Math.floor(Math.random() * (this.input_value_change_timer_max[index] - this.input_value_change_timer_min[index] + 1) + this.input_value_change_timer_min[index]);
    }

    _inputsGetChangeStep() {
        const max = Math.abs(this.input_value_change_threshold * 2.0);
        const min = Math.abs(this.input_value_change_threshold / 2.0);
        return Math.abs(Math.random() * (max - min) + min).toFixed(4);
    }

    _inputChange(index) {
        const old_input_value = parseFloat(this.inputs_value[index]);
        var new_input_value = (old_input_value + (this.input_value_change_direction[index] * this._inputsGetChangeStep())).toFixed(4);

        if (new_input_value < this.min_input_value ) {
            new_input_value = this.min_input_value;
            this.input_value_change_direction[index] = 1;
        } else if (new_input_value > this.max_input_value) {
            new_input_value = this.max_input_value;
            this.input_value_change_direction[index] = -1;
        }

        if (Math.abs(new_input_value - this.last_reported_threshold_value[index]) >= this.input_value_change_threshold) {
            if (this.input_value_change_threshold_callback != null) {
                this.input_value_change_threshold_callback(index, new_input_value);
            }
        }

        const new_input_value_in_range = (new_input_value >= this.input_value_range_low && new_input_value <= this.input_value_range_high) ? true : false;

        if (!this.last_input_value_in_range[index] && new_input_value_in_range) {
            if (this.within_range_callback != null) {
                this.within_range_callback(index, new_input_value);
            } 
        } else if (this.last_input_value_in_range[index] && !new_input_value_in_range) {
            if (this.out_of_range_callback != null) {
                this.out_of_range_callback(index, new_input_value);
            } 
        }

        this.last_input_value_in_range[index] = new_input_value_in_range;
        this.inputs_value[index] = new_input_value;

        this.input_value_change_timer[index] = setTimeout(this._inputChange.bind(this), this._inputsGetChangeTimeout(index), index);
    }

    inputsSetIntervalCallback(cb, timeout) {
        if (this.interval_timer != null) {
            clearInterval(this.interval_timer);
            this.interval_timer = null;
        }

        if (cb != null) {
            this.interval_callback = cb;

            this.interval_timer = setInterval(() => {
                for (var i = 0; i < 7; i++) {
                    this.interval_callback(i, this.inputs_value[i]);
                }
            }, timeout);
        }
    }

    inputsSetWithinRangeCallback(cb) {
        this.within_range_callback = cb;
    }

    inputsSetOutOfRangeCallback(cb) {
        this.out_of_range_callback = cb;
    }

    inputsSetThresholdCallback(cb) {
        this.input_value_change_threshold_callback = cb;
    }

    inputsSetRange(min, max) {
        this.input_value_range_low = min;
        this.input_value_range_high = max;
    }

    inputsSetThrehold(threshold) {
        this.input_value_change_threshold = threshold;
    }

    inputsGetAll() {
        const inputs_value = [...this.inputs_value];

        return inputs_value;
    }

    inputsGetOne(index) {
        if (!Number.isInteger(index) || index < 0 || index > 6) {
            throw new Error('index must be an integer between 0 and 6');
        }

        const inputs_value = this.inputsGetAll();

        return inputs_value[index];
    }

    relaySetPulseWitdth(pulse_width_array) {
        if (!Array.isArray(pulse_width_array) || pulse_width_array.length != 2) {
            throw new Error('pulse_width_array must be an array of 2 elements');
        }

        for (var i = 0; i < 2; i++) {
            if (Number.isInteger(pulse_width_array[i]) && pulse_width_array[i] < 0) {
                this.relay_pulse_width[i] = pulse_width_array[i];
            } else {
                // emod-controller-js-bindings equivalent method does implicit conversion to 0 if value is not a valid integer.
                this.relay_pulse_width[i] = 0;
            }
        }
    }

    relaySetOne(index, value) {
        if (!Number.isInteger(index) || index < 0 || index > 1) {
            throw new Error('index must be an integer between 0 and 1');
        }

        if (typeof value !== "boolean") {
            throw new Error('value must be a boolean');
        }

        this.relay_status[index] = value;

        if (this.relay_status[index] && this.relay_pulse_width[index] != 0) {
            setTimeout(() => {
                this.relay_status[index] = false;
            }, this.relay_pulse_width[index]);
        }
    }

    relaySetAll(value) {
        if (typeof value !== "boolean") {
            throw new Error('value must be a boolean');
        }

        for (var i = 0; i < 2; i++) {
            this.relaySetOne(i, value);
        }
    }

    relaySet(new_relay_status) {
        if (!Array.isArray(new_relay_status) || new_relay_status.length != 2) {
            throw new Error('new_relay_status must be an array of 2 elements');
        }

        for (var i = 0; i < 8; i++) {
            if (typeof new_relay_status[i] === "boolean") {
                this.relaySetOne(i, new_relay_status[i]);
            }
        }
    }

    relayGetAll() {
        const relay_status = [...this.relay_status];

        return relay_status;
    }

    relayGetOne(index) {
        if (!Number.isInteger(index) || index < 0 || index > 1) {
            throw new Error('index must be an integer between 0 and 1');
        }

        const relay_status = this.relayGetAll();

        return relay_status[index];
    }
}


class SingletonAI7PR2 {
    constructor() {
        throw new Error('Use getInstance(num)');
    }
    
    static getInstance(num) {
        if (!Number.isInteger(num) || num < 1 || num > 5) {
            throw new Error('Module number must be an integer between 1 and 5');
        }

        if (!SingletonAI7PR2.instance) {
            SingletonAI7PR2.instance = new Array(5).fill(null);;
        }

        if (!SingletonAI7PR2.instance[num-1]) {
            SingletonAI7PR2.instance[num-1] = new AI7PR2();
        }

        return SingletonAI7PR2.instance[num-1];
    }
}

module.exports = SingletonAI7PR2;
